AWSTemplateFormatVersion: 2010-09-09
Description: For AWSome PetClinic container CI/CD demo
Parameters:
  AWSomePetClinicContainerName:
    Type: String
    Default: awsome-petclinic-test
    Description: Container name to be populated by CodeBuild
Resources:    
  AWSomePetClinicECR:
    Type: AWS::ECR::Repository
  AWSomePetClinicCodeCommit:
    Type: AWS::CodeCommit::Repository
    Properties:
      RepositoryDescription: CodeCommit Repo for AWSome PetClinic CICD Pipeline
      RepositoryName: petclinic-code-repo
  AWSomePetClinicCodeBuildServiceRole:
    Type: AWS::IAM::Role
    Properties:
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser
        - arn:aws:iam::aws:policy/AmazonECS_FullAccess         
        - arn:aws:iam::aws:policy/AWSCodeCommitFullAccess 
        - arn:aws:iam::aws:policy/AWSLambdaFullAccess 
        - arn:aws:iam::aws:policy/AWSCodeDeployFullAccess         
        - arn:aws:iam::aws:policy/AWSCodePipelineFullAccess 
        - arn:aws:iam::aws:policy/AWSCodeBuildAdminAccess 
        - arn:aws:iam::aws:policy/CloudWatchEventsFullAccess 
        - arn:aws:iam::aws:policy/AWSCodeDeployRoleForECS
      Policies:
        - PolicyName: AWSomePetClinicCodeBuildServiceRolePolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - codecommit:GitPull
                  - codecommit:PostCommentForPullRequest
                  - codecommit:UpdatePullRequestApprovalState
                Resource: !GetAtt AWSomePetClinicCodeCommit.Arn
              - Effect: Allow
                Action:
                  - logs:CreateLogGroup
                  - logs:CreateLogStream
                  - logs:PutLogEvents
                Resource: '*'
              - Effect: Allow
                Action:
                  - s3:GetObject
                  - s3:GetObjectVersion
                  - s3:PutObject
                  - s3:GetBucketAcl
                  - s3:GetBucketLocation
                Resource: '*'
              - Effect: Allow
                Action:
                  - securityhub:BatchImportFindings
                Resource: '*'
              - Effect: Allow
                Action:
                  - ecr:GetDownloadUrlForLayer
                  - ecr:BatchGetImage
                  - ecr:BatchCheckLayerAvailability
                  - ecr:PutImage
                  - ecr:InitiateLayerUpload
                  - ecr:UploadLayerPart
                  - ecr:CompleteLayerUpload
                Resource: !GetAtt AWSomePetClinicECR.Arn
              - Effect: Allow
                Action:
                  - ssm:GetParameters
                Resource: !Sub "arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter/petstore/*"
        - PolicyName: TestPolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - codebuild:CreateReportGroup
                  - codebuild:CreateReport
                  - codebuild:UpdateReport
                  - codebuild:BatchPutTestCases
                  - codebuild:BatchPutCodeCoverages
                  - ecs:DescribeServices
                Resource: '*'
        - PolicyName: DeployPolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - autoscaling:*
                  - codedeploy:*
                  - ec2:*
                  - lambda:*
                  - ecs:*
                  - elasticloadbalancing:*
                  - iam:AddRoleToInstanceProfile
                  - iam:CreateInstanceProfile
                  - iam:CreateRole
                  - iam:DeleteInstanceProfile
                  - iam:DeleteRole
                  - iam:DeleteRolePolicy
                  - iam:GetInstanceProfile
                  - iam:GetRole
                  - iam:GetRolePolicy
                  - iam:ListInstanceProfilesForRole
                  - iam:ListRolePolicies
                  - iam:ListRoles
                  - iam:PassRole
                  - iam:PutRolePolicy
                  - iam:RemoveRoleFromInstanceProfile
                  - s3:*
                  - ssm:*
                Resource: '*'

      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal: { Service: codebuild.amazonaws.com }
            Action:
              - sts:AssumeRole
          - Effect: Allow
            Principal: { Service: codedeploy.amazonaws.com }
            Action:
              - sts:AssumeRole
  
  AWSomePetClinicCodePipelineServiceRole:
    Type: AWS::IAM::Role
    Properties:
      Policies:
        - PolicyName: AWSomePetClinicCodePipelineServiceRolePolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - codecommit:CancelUploadArchive
                  - codecommit:GetBranch
                  - codecommit:GetCommit
                  - codecommit:GetUploadArchiveStatus
                  - codecommit:UploadArchive
                Resource: !GetAtt AWSomePetClinicCodeCommit.Arn
              - Effect: Allow
                Action:
                  - sns:Publish
                  - cloudeploy:*
                  - cloudwatch:*
                  - s3:*
                  - ecs:*
                Resource: '*'
              - Effect: Allow
                Action:
                  - codebuild:BatchGetBuilds
                  - codebuild:StartBuild
                Resource: !GetAtt AWSomePetClinicCodeBuild.Arn
              - Effect: Allow
                Action:
                  - iam:GetRole
                  - iam:PassRole
                Resource: '*'
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal: { Service: codepipeline.amazonaws.com,  }
            Action:
              - sts:AssumeRole
  AWSomePetClinicCodePipelineArtifactBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub 'awsome-petclinic-codepipeline-artifacts-${AWS::AccountId}'
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      VersioningConfiguration:
        Status: Enabled
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: AES256

  AWSomePetClinicPullRequestCodeBuild:
    Type: AWS::CodeBuild::Project
    Properties:
      Artifacts:
        Type: NO_ARTIFACTS
      Description: AWSome PetClinic Pull Request build
      Environment:
        ComputeType: BUILD_GENERAL1_LARGE
        Image: aws/codebuild/standard:4.0
        PrivilegedMode: True
        Type: LINUX_CONTAINER
        EnvironmentVariables:
          - Name: docker_img_name
            Value: !Ref AWSomePetClinicContainerName
          - Name: docker_tag
            Value: latest
          - Name: ecr_repo
            Value: !Sub '${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/${AWSomePetClinicECR}'
      LogsConfig:
        CloudWatchLogs:
          Status: ENABLED
      Name: !Sub 'awsome-petclinic-pr-project-${AWS::AccountId}'
      ServiceRole: !GetAtt AWSomePetClinicCodeBuildServiceRole.Arn
      Source:
        Type: CODECOMMIT
        Location: !GetAtt AWSomePetClinicCodeCommit.CloneUrlHttp
        BuildSpec: pr-buildspec.yml


  AWSomePetClinicCodeBuild:
    Type: AWS::CodeBuild::Project
    Properties:
      Artifacts:
        Type: CODEPIPELINE
      Description: AWSome PetClinic build
      Environment:
        ComputeType: BUILD_GENERAL1_LARGE
        Image: aws/codebuild/amazonlinux2-x86_64-standard:3.0
        PrivilegedMode: True
        Type: LINUX_CONTAINER
        EnvironmentVariables:
          - Name: docker_img_name
            Value: !Ref AWSomePetClinicContainerName
          - Name: docker_tag
            Value: latest
          - Name: ecr_repo
            Value: !Sub '${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/${AWSomePetClinicECR}'
      LogsConfig:
        CloudWatchLogs:
          Status: ENABLED
      Name: !Sub 'awsome-petclinic-build-project-${AWS::AccountId}'
      ServiceRole: !GetAtt AWSomePetClinicCodeBuildServiceRole.Arn
      Source:
        Type: CODEPIPELINE

  AWSomePetClinicCodePipeline:
    Type: AWS::CodePipeline::Pipeline
    Properties:
      ArtifactStore:
        Location: !Ref AWSomePetClinicCodePipelineArtifactBucket
        Type: S3
      Name: !Sub 'awsome-petclinic-scan-cicd-pipeline-${AWS::AccountId}'
      RestartExecutionOnUpdate: True
      RoleArn: !GetAtt AWSomePetClinicCodePipelineServiceRole.Arn
      Stages:
        -
          Name: Source
          Actions:
            -
              Name: SourceAction
              ActionTypeId:
                Category: Source
                Owner: AWS
                Version: '1'
                Provider: CodeCommit
              Configuration:
                RepositoryName: !GetAtt AWSomePetClinicCodeCommit.Name
                BranchName: master
              OutputArtifacts:
                -
                  Name: SourceOutput
              RunOrder: 1
        -
          Name: Build
          Actions:
            -
              InputArtifacts:
                -
                  Name: SourceOutput
              OutputArtifacts:
                -
                  Name: BuildOutput
              Name: BuildAction
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: '1'
                Provider: CodeBuild
              Configuration:
                ProjectName: !Ref AWSomePetClinicCodeBuild
                PrimarySource: SourceOutput
              RunOrder: 1
        - Name: ManualApproval
          Actions:
            - Name: Manual
              ActionTypeId:
                Category: Approval
                Owner: AWS
                Version: 1
                Provider: Manual
              Configuration:
                NotificationArn: arn:aws:sns:us-east-1:661904874735:DeploymentApprovalTopic

        - Name: DeployTest
          Actions:
            - Name: Deploy
              ActionTypeId:
                  Category: Deploy
                  Owner: AWS
                  Provider: ECS
                  Version: '1'
              Configuration:
                ClusterName: 'awsomepletclinic-stack-awsomecluster44878E37-z4VccgaLdP1Q'
                ServiceName: 'awsomepletclinic-stack-awsomefgserviceService3C643573-1RQPDWC02KLG0'
              InputArtifacts:
                - Name: BuildOutput
              RunOrder: 1



  
  AWSomePetClinicCoverageReportGroup:
    Type: AWS::CodeBuild::ReportGroup
    Properties:
      Name: awsomepetclinic-coverage-report
      Type: CODE_COVERAGE
      ExportConfig:
        ExportConfigType: NO_EXPORT      

  AWSomePetClinicDependencyCheckReportGroup:
    Type: AWS::CodeBuild::ReportGroup
    Properties:
      Name: awsomepetclinic-dependencycheck-report
      Type: TEST
      ExportConfig:
        ExportConfigType: NO_EXPORT

  AWSomePetStoreCloudWatchEventsCodeBuildRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - events.amazonaws.com
            Action: sts:AssumeRole
      Path: /
      Policies:
        - PolicyName: events-invoke-codebuild
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Effect: Allow
                Resource: !GetAtt AWSomePetClinicCodeBuild.Arn
                Action: 
                  - codebuild:StartBuild


  AWSomePetStorePullRequestTriggerCodeBuildRule:
    Type: AWS::Events::Rule
    Properties: 
      Description: 'Rule to trigger build from CodeCommit pull request'
      EventPattern:
        source:
          - aws.codecommit
        detail-type:
          - 'CodeCommit Pull Request State Change'
        detail:
          event:
            - pullRequestCreated
            - pullRequestSourceBranchUpdated
        resources:
          - !GetAtt AWSomePetClinicCodeCommit.Arn
      State: ENABLED
      Targets: 
        - Arn: !GetAtt AWSomePetClinicPullRequestCodeBuild.Arn
          Id: codebuild
          RoleArn: !GetAtt AWSomePetStoreCloudWatchEventsCodeBuildRole.Arn
          InputTransformer:
            InputTemplate: |
              {
                "sourceVersion": <sourceVersion>,
                "artifactsOverride": {"type": "NO_ARTIFACTS"},
                "environmentVariablesOverride": [
                   {
                       "name": "PULL_REQUEST_ID",
                       "value": <pullRequestId>,
                       "type": "PLAINTEXT"
                   },
                   {
                       "name": "REPOSITORY_NAME",
                       "value": <repositoryName>,
                       "type": "PLAINTEXT"
                   },
                   {
                       "name": "SOURCE_COMMIT",
                       "value": <sourceCommit>,
                       "type": "PLAINTEXT"
                   },
                  {
                       "name": "SOURCE_REFERENCE",
                       "value": <sourceReference>,
                       "type": "PLAINTEXT"
                   },
                  {
                       "name": "DESTINATION_REFERENCE",
                       "value": <destinationReference>,
                       "type": "PLAINTEXT"
                   },                   
                   {
                       "name": "DESTINATION_COMMIT",
                       "value": <destinationCommit>,
                       "type": "PLAINTEXT"
                   },
                   {
                      "name" : "REVISION_ID",
                      "value": <revisionId>,
                      "type": "PLAINTEXT"
                   }
                ]
              }
            InputPathsMap:
              sourceVersion: "$.detail.sourceCommit"
              pullRequestId: "$.detail.pullRequestId"
              repositoryName: "$.detail.repositoryNames[0]"
              sourceCommit: "$.detail.sourceCommit"
              sourceReference: "$.detail.sourceReference"
              destinationReference: "$.detail.destinationReference"
              destinationCommit: "$.detail.destinationCommit"
              revisionId: "$.detail.revisionId"